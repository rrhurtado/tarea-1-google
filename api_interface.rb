# superclase de la que heredan las distintas interfaces segun el patron template
class APIInterface
  def get_org(org_name)
    # recibe una string con el nombre de la organizacion y retorna un hash con el siguiente formato:
    # {"company"=>string, "website"=>string, "location"=>string, "email"=>string, "followers"=>integer,
    # "following"=>integer, "repo_list"=>{"name"=>string, "private?"=>boolean}}
  end

  def get_repo(org_name, repo_name)
    # recibe strings con el nombre de la organizacion y del repo y retorna un hash con el siguiente formato:
    # {"description"=>string, "website"=>string, "languages"=>string array,
    # fork_count"=>integer, "watchers"=>integer, "stargazers"=>integer, "open_issues"=>integer,
    # "latest_commit"=>{hash=>string, contributor=>string, date=>DateTime, parent=>string}}
  end

  def aggregate_org_repos(org_name)
    # recibe una string con el nombre de la organizacion y retorna un hash con el siguiente formato:
    # {"repo_count"=>integer, "fork_count"=>integer, "watchers"=>integer, "stargazers"=>integer,
    # "open_issues"=>integer}
  end

  def get_latest_commit(org_name, repo_name)
    # recibe strings con el nombre de la organizacion y del repo y retorna un hash con el siguiente formato:
    # {hash=>string, contributor=>string, date=>DateTime, parent=>string}
  end

  def list_issues(org_name, repo_name)
    # recibe strings con el nombre de la organizacion y del repo y retorna un ARREGLO DE HASHES
    # con el siguiente formato:
    # [{"author"=>string, "date"=>DateTime, "assignees"=>string array, "state"=>boolean, "labels"=>string array}]
  end

  def list_pulls(org_name, repo_name)
    # recibe strings con el nombre de la organizacion y del repo y retorna un ARREGLO DE HASHES
    # con el siguiente formato:
    # [{"open?"=>boolean, "base branch"=>string}]
  end

  def list_contributors(org_name, repo_name)
    # recibe strings con el nombre de la organizacion y del repo y retorna un ARREGLO DE HASHES
    # con el siguiente formato:
    # [{"username"=>string, "contributions"=>integer}]
  end

  def get_user(username)
    # recibe una string con el nombre del usuario y retorna un hash con el siguiente formato:
    # {"name"=>string, "company"=>string, "website"=>string, "location"=>string, "email"=>string,
    # "public_repos"=>integer, "created_at"=>DateTime}
  end

  def upload_readme(content)
    # recibe una string con el contenido a postear como el archivo README del repositorio
  end
end
