#!/usr/bin/env ruby

# -----------------------------Configuration------------------------------------
# Function by Robert Kajic
# http://stackoverflow.com/questions/9236673/ruby-gems-in-stand-alone-ruby-scripts
def load_gem(name, version = nil)
  # Needed if your ruby version is less than 1.9
  require 'rubygems'
  begin
    gem name, version
  rescue LoadError
    version = "--version '#{version}'" unless version.nil?
    system("gem install #{name} #{version}")
    Gem.clear_paths
    retry
  end

  require name
end

# Import required gems
load_gem 'github_api'

# -------------------------------Definitions------------------------------------
# RF1
def github_logged_org_instance_from_input
  success = false
  until success
    begin
      user = ENV['GITHUB_USER']
      pass = ENV['GITHUB_PASS']
      raise 'Please export the needed environment variables GITHUB_USER and GITHUB_PASS' if user.nil? || pass.nil?
      puts 'Enter GitHub organization name... (Leave blank for default)'
      org = gets.chomp
      org = 'IIC2113-2016-2' if org == ''
      github = Github.new basic_auth: "#{user}:#{pass}",
                          user: org,
                          repo: 'finite_machine'
      github.repos.list # Check for successful login
      success = true
    rescue Github::Error::Unauthorized
      puts 'Wrong username or password. Please try again...'
    rescue Github::Error::NotFound
      puts '404: Repo not found!'
    end
  end
  return github
end

# RF2
def github_repo_data(github, repo)
  puts 'Repo name: ' + repo.name
  puts 'Private: ' + repo.private.to_s
  commits = github.repos.commits.all github.user, repo.name
  puts 'N° commits: ' + commits.count.to_s
  puts 'Forks: ' + repo.forks_count.to_s
  puts 'Stargazers: ' + repo.stargazers_count.to_s
  puts 'Watchers: ' + repo.watchers_count.to_s
  puts 'Open Issues: ' + repo.open_issues_count.to_s
  puts ''
end

def github_list_repos(github)
  repos = github.repos.list
  repos.each do |repo|
    github_repo_data(github, repo)
  end
end

# RF3
def github_check_repo(github, repository)
  repos = github.repos.list
  repo = (repos.select { |r| r[:name] == repository }).first
  github_repo_data(github, repo)
end

# RF4
def github_aggregated_data(forks, stargazers, watchers, open_issues)
  puts 'Forks: ' + forks.to_s
  puts 'Stargazers: ' + stargazers.to_s
  puts 'Watchers: ' + watchers.to_s
  puts 'Open Issues: ' + open_issues.to_s
  puts ''
end

def github_aggregated(github)
  forks = 0
  stargazers = 0
  watchers = 0
  open_issues = 0
  repos = github.repos.list
  repos.each do |repo|
    forks += repo.forks_count
    stargazers += repo.stargazers_count
    watchers += repo.watchers_count
    open_issues += repo.open_issues_count
  end
  github_aggregated_data(forks, stargazers, watchers, open_issues)
end

# RF5
def github_commit_data(github, repo)
  commit = (github.repos.commits.all github.user, repo.name)[0]
  puts 'Repo name: ' + repo.name.to_s
  puts 'Last commit hash: ' + commit.sha.to_s
  puts 'Last commit date: ' + commit.commit.committer.date.to_s
  puts 'Last commit author: ' + commit.commit.author.name.to_s
  puts ''
end

def github_last_commits(github)
  repos = github.repos.list
  repos.each do |repo|
    github_commit_data(github, repo)
  end
end

# RF6
def github_issue_data(issue)
  puts 'Issue title: ' + issue.title.to_s
  puts 'Issue author: ' + issue.user.login.to_s
  puts 'Issue creation date: ' + issue.created_at.to_s
  puts 'Last issue update: ' + issue.updated_at.to_s
  puts 'Issue n° of comments: ' + issue.comments.to_s
  puts ''
end

def github_issues(github)
  repos = github.repos.list
  repos.each do |repo|
    issues = github.issues.list user: github.user, repo: repo.name, state: 'all'
    (issues.select { |i| i[:state] == 'open' }).each do |issue|
      github_issue_data(issue)
    end
  end
end

# ---------------------------Main Execution code--------------------------------
github = github_logged_org_instance_from_input
# github_list_repos(github) # RF2
# github_check_repo(github, 'syllabus') # RF3
# github_aggregated(github) # RF4
# github_last_commits(github) # RF5
# github_issues(github) # RF6
# RF7
# RF8
# RF9
# RF10
